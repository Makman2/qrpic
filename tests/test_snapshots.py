import os
from io import BytesIO

import pytest

from qrpic.main import main, create_arg_parser


@pytest.mark.parametrize(
    'filename_logo, test_url, filename_expected, qrpic_args',
    (
        [
            (
                f'snapshots/case1/logo.svg',
                'https://gitlab.com/Makman2/qrpic',
                f'snapshots/case1/expected-shell-{shell}.svg',
                ['--shell', shell],
            )
            for shell in ('none', 'viewbox', 'boundary-box', 'convex')
        ] + [
            (
                f'snapshots/case2/logo.svg',
                'https://gitlab.com/Makman2/qrpic',
                f'snapshots/case2/expected-shell-{shell}.svg',
                ['--shell', shell, '--svg-area', '0.1'],
            )
            for shell in ('none', 'viewbox', 'boundary-box', 'convex')
        ]
    )
)
def test_snapshot(filename_logo, test_url, filename_expected, qrpic_args):
    testpath = os.path.dirname(os.path.realpath(__file__))

    output = BytesIO()

    args = create_arg_parser().parse_args(list(qrpic_args) + [test_url, os.path.join(testpath, filename_logo)])
    args.out = output
    main(args)

    with open(os.path.join(testpath, filename_expected), 'rb') as fl:
        expected = fl.read()

    output.seek(0)
    assert output.read() == expected
